FROM ubuntu:latest
RUN apt update
RUN apt install -y build-essential cmake qt5-default 
COPY . /source
WORKDIR /source/build
RUN cmake .. 
RUN make
ENTRYPOINT [ "/source/encoder" ]

