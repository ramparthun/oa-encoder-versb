#ifndef DECODER_H
#define DECODER_H
#include <iostream>
#include <fstream>
#include "json.hpp"
#include <QString>
class decoder
{
    nlohmann::json codingtable;
public:
    decoder();
    QString code(const QString &input);
    QString decode(const QString &input);
};

#endif // DECODER_H
