#include "decoder.h"

decoder::decoder()
{
    std::string path,rawfile,assist;
    bool pathfound=false;
    while(not pathfound){
        std::cout<<"coding table: "<<std::endl;
        std::getline(std::cin,path);
        try {
            std::ifstream input(path);
            if(input.is_open()){
                pathfound=true;
                while(not input.eof()){
                    input>>assist;
                    rawfile+=assist;
                    rawfile+=" ";
                }
            }else {
                throw "invalid input";
            }
            codingtable=nlohmann::json::parse(rawfile);
            input.close();
        } catch (...) {
            std::cout<<"ERROR: Invalid coding table"<<std::endl;
        }
    }
}

QString decoder::code(const QString &input)
{
    QString coded;
    bool found=false;
    for(auto it:input){
       if(codingtable.find(QString(it).toStdString())!=codingtable.end()){
            found=true;
            coded.push_back(QString::fromStdString(codingtable[QString(it).toStdString()]));
        }
    }
    if (not found){
        coded="INVALID INPUT!";
    }
    return coded;
}

QString decoder::decode(const QString &input)
{
   QString localinput=input;
   bool found=false;
    QString decoded;
    while(localinput.length()>0){
        for(auto it:codingtable.items()){
            if(localinput.startsWith(QString::fromStdString(it.value()))){
                found=true;
                decoded.push_back(QString::fromStdString(it.key()));
                QString assist=QString::fromStdString(it.value());
                localinput.remove(0,assist.length());
                break;
            }
        }
        if(not found){
            break;
        }
    }
    if(found){
        return decoded;
    }else{
        return "CODE NOT FOUND!";
    }
}



