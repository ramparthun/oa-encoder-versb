#include "decoder.h"
#include <istream>
#include <QString>
#include <string>
#include <QStringList>
#include <fstream>
QStringList get_new_input(){
    std::string assist;
    QString input;
    std::cin.clear();
    std::cout<<std::endl<<"COMMAND: "<<std::endl;
    std::getline(std::cin,assist);
    input=QString::fromStdString(assist).toLower();
    return input.split(" ",QString::SkipEmptyParts);
}
int main(){
    decoder coder;
    QStringList line=get_new_input();
    bool exit=false;

    while (not exit){
        if(line[0]=="exit"){
            exit=true;
        }else if(line[0]=="encode"){
            for(int i=1;i<line.length();i++) {
                std::cout << coder.code(line[i]).toStdString();
            }
            line=get_new_input();
        }else if(line[0]=="decode"){
            for(int i=1;i<line.length();i++){
                    std::cout<<coder.decode(line[i]).toStdString();
            }
            line=get_new_input();
        }else if(line[0]=="encode_file"){
            try {
                std::ifstream inputfile(line[1].toStdString());
                std::ofstream output(line[2].toStdString());
                if(inputfile.is_open()&&output.is_open()){
                    while(not inputfile.eof()) {
                        std::string assist;
                        inputfile>>assist;
                        if(coder.code(QString::fromStdString(assist)).toStdString()=="INVALID INPUT!"){
                            std::cout<<"INVALID INPUT"<<std::endl;
                            break;
                        }else {
                            output << coder.code(QString::fromStdString(assist)).toStdString();
                        }
                    }
                    inputfile.close();
                    output.close();
                    line=get_new_input();
                }else {
                    throw "invalid file";
                }
            } catch (...) {
                std::cout<<"INVALID INPUT FILE"<<std::endl;
                line=get_new_input();
            }
        }else if(line[0]=="decode_file"){
            try {
                std::ifstream inputfile(line[1].toStdString());
                std::ofstream output(line[2].toStdString());
                if(inputfile.is_open()&&output.is_open()){
                    while(not inputfile.eof()) {
                        std::string assist;
                        inputfile>> assist;
                        if(coder.decode(QString::fromStdString(assist)).toStdString()=="INVALID INPUT!"){
                            std::cout<<"INVALID INPUT"<<std::endl;
                            break;
                        }else {
                            output << coder.decode(QString::fromStdString(assist)).toStdString();
                        }
                    }
                    inputfile.close();
                    output.close();
                    line=get_new_input();
                }else {
                    throw "invalid file";
                }
            } catch (...) {
                std::cout<<"INVALID INPUT FILE"<<std::endl;
                line=get_new_input();
            }
        }else{
            std::cout<<"INVALID COMMAND!"<<std::endl;
            line=get_new_input();
        }

    }
    if(exit){
        std::cout<<"KTHXBYE";
    }
    return 0;
}
